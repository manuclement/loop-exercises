<?php

// EXERCICE 6

$hash = "";

echo "Exercice 6 :\n\n";

for ($i=0;$i<50;$i++) {
    $hash.="#";
    echo "$hash \n";
}

// EXERCICE 7

echo "\n\n Exercice 7 - Suite de Fibonacci :\n\n";

$number1 = 0;
$number2 = 1;

$result = 0;

while($number1 <= 5000) {
    echo "$number1, ";
    $result = $number2 + $number1;
    $number1 = $number2;
    $number2 = $result;
}

// EXERCICE 8

$k = 0;
$number = 2;

echo "\n\nExercice 8 - 20 premiers nombres premiers : ";

while($k < 20) {
    $is_first = true;

    for ($i=2;$i<$number;$i++) {
        if ($number%$i == 0 )
            $is_first = false;
    }

    if ($number < 2)
        $is_first = false;

    if ($is_first) {
        echo "$number, ";
        $k++;
    }
    $number++;
}

echo "\n\n";
?>