<?php


// EXERCICE 1

$numbers_fr = array("zéro", "un" ,"deux", "trois", "quatre", "cinq", "six", "sept", "huit", "neuf", "dix", "onze", "douze", "treize", "quatorze", "quinze", "seize", "dix-sept", "dix-huit", "dix-neuf","vingt");

$number = intval(readline("Exercice 1 - Nombre: "));

if ($number < 21 && $number > -1 && is_int($number)) {
    echo "En toutes lettres : $numbers_fr[$number]\n";
}

else
    echo "Le nombre doit être entre 0 et 20\n";


// EXERCICE 2

$number1 = intval(readline("Exercice 2 - Premier nombre: "));
$number2 = intval(readline("Exercice 2 - Deuxième nombre: "));

for ($i=$number1+1;$i<$number2;$i++) {
    echo "$i \n";
}

// EXERCICE 3

$number = intval(readline("Exercice 3 - Nombre: "));
$result = $number/3;
if ($number%3 > 0)
    echo "Nombre non divisible par 3\n";
else
    echo "Nombre divisible par 3 ($number/3 = $result)\n";


// EXERCICE 4

$number = intval(readline("Exercice 4 - Nombre: "));

$is_first = true;

for ($i=2;$i<$number;$i++) {
    if ($number%$i == 0 )
        $is_first = false;
}

if ($number < 2)
    $is_first = false;

echo $is_first ? "$number est un nombre premier" : "$number n'est pas un nombre premier\n";


// EXERCICE 5

echo "\n\n Exercice 5.1 - Table de multiplication de 1 :\n\n";

for ($i=1;$i<11;$i++) {
    $result = $i*1;
    echo "$i x 1 = $result\n";
}

echo "\n\n Exercice 5.1 - Table de multiplication de 6 :\n\n";

for ($i=1;$i<11;$i++) {
    $result = $i*6;
    echo "$i x 6 = $result\n";
}

$number = intval(readline("\n\n Exercice 5.2 - Nombre: "));

for ($i=1;$i<11;$i++) {
    $result = $i*$number;
    echo "$i x $number = $result\n";
}

for ($j=1;$j<11;$j++) {

    echo "\n\n Exercice 5.3 - Table de multiplication de $j :\n\n";

    for ($i=1;$i<11;$i++) {
        $result = $i*$j;
        echo "$i x $j = $result\n";
    }

}

?>